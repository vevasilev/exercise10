import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Exercise10 {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        System.out.print("Enter the date in the format dd.MM.yyyy: ");
        LocalDate dateTime = LocalDate.parse(new Scanner(System.in).nextLine(), formatter);
        System.out.println(dateTime.isLeapYear() ? "The year " + dateTime.getYear() + " is a leap year." : "The year " + dateTime.getYear() + " is not a leap year.");
    }
}